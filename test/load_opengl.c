
#include <evg/evg.h>
#include <dlfcn.h>
#include <assert.h>

int main() {
#ifdef __linux__
    void* handle = dlopen ("devices/opengl.so", RTLD_LOCAL | RTLD_LAZY);

    assert(handle != NULL);

    evgDeviceFunction* factory = (evgDeviceFunction*) dlsym (handle, "evg_device_descriptor");
    assert(handle != NULL);
    
    const evgDeviceDescriptor* desc = factory();
    assert(desc != NULL);
    assert(evg_device_descriptor_valid (desc));

    dlclose (handle);
    return 0;
#else
    return 0;
#endif
}
