# Element Video Graphics

EVG: An ultra LV2 extension for realtime video processing with liberal licensing. (proposal draft stage)

**State of API** - device plugins work, but things like type names and interfaces 
will change.  There is a [video section](https://gitlab.com/lvtk/lvtk/-/tree/v3/demo/demo/video) in 
the LVTK GUI c++ demo that uses libevg.

## Why do this?
Ever heard of a Video effect plugin standard?  Let me know if you have -MRF

## Highest Level Architecture
Defined in RDF. Possibilities:
  - Inherit lv2:Plugin (or new plugin type) in RDF for "sources"
    - Need some way to add new port type(s) for video frames
    - data type for video uint64_t representing some kind of
      texture or ID for ins and outs.
  - Typical plugin hypothetically in a tick or expose callback:
    1) load input/output textures
    2) load other XYZ's.
    2) run a custom or default shader program
    3) update the output texture(s) somehow.
  - Inherit lv2:UI in RDF for Gui's (if needed)
  - Ship video fx as lv2 plugins

_I'm not a video engine guru, so the details of what video ports are... I don't know. But will find out!_

## Benefits of LV2 Piggy backing
  - LV2 Hosts could incorporate easily.
  - Searchability with Lilv
  - Same GUI format and expectations.
  - Video streams have audio too :)

## Possible Plugin Types
_think about things you'd see in a video editor or what it takes to produce a pro-looking OBS stream_
- **Device**: graphics backends (OpenGL Vulkan, etc..)
  - Devices provide a common api for lowish level graphics rendering in realtime.
    - loading textures, shaders, projections
    - saving context state (just like in GUI drawing)
    - provide "swap" system with native os windows (needed for actual video display)

- **Source**: types of media sources
  - Videos, Images, Solids
  - Text Source
  - Credit Roll
  - Vector sources
  - Video display endpoint.
    - A dumb plugin with video display gui
  - Generators
  - Visual FX
  - Transitions
    - Acheived with a 2 in 1 plugin

## Support headers and maybe lib
There are alot of C Interfaces that look like various feature structs.
Something to thinly wrap these in C/C++.

## Nice To Have's
- **Shader Language/Compiler**: See the OBS graphics code for what I mean and why.
  - Alot of effects and generators are shader programs.
  - Boost has nice templates for writing C-Style preprocessors <-- essential and professional
  - Hypothetical: The compiler would take input from `*.lvsl` scripts and "render" them
    to GLSL, vulkan shader language... defintely possible as this is how OBS does it.
  - I learn by example, hence the OBS reference above.

## Who would actually use it?
I would... and many others I assume over time. [Element](https://github.com/kushview/element) 
one day will have video nodes using this very system regardless. The code here was
originally an RND project to prove to myself video nodes are possible. The way I see it these days, 
this could be a cool extension to LV2 and utilize all of it's exosystem at the same time.
